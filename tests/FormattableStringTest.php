<?php

namespace Dropkick\Core\Formattable;

use Dropkick\Core\Formattable\Formatter\GenericFormatter;
use PHPUnit\Framework\TestCase;

class FormattableStringTest extends TestCase {

  public function testGet() {
    $message = 'test message';
    $arguments = ['test' => 'message'];

    $string = new FormattableString($message, $arguments);
    $this->assertEquals($string->getString(), $message);
    $this->assertEquals($string->getPlaceholders(), $arguments);
  }

  public function testFormatter() {
    $message = 'Test %s';
    $arguments = ['message'];

    $string = new FormattableString($message, $arguments, new DemoFormatter());
    $this->assertEquals('Test message', (string) $string);
  }

  public function testDefaultFormatter() {
    $message = 'Test %s';
    $arguments = ['message'];

    FormattableString::setDefaultFormatter(new DemoFormatter());
    $string = new FormattableString($message, $arguments);
    $this->assertEquals('Test message', (string) $string);
    FormattableString::setDefaultFormatter();
  }

  public function testCreate() {
    $message = 'Test %s';
    $arguments = ['message'];

    FormattableString::setDefaultFormatter(new DemoFormatter());
    $string = FormattableString::create($message, $arguments);
    $this->assertEquals('Test message', (string) $string);
    $this->assertEquals(DemoString::class, get_class($string));

    FormattableString::setDefaultFormatter();
  }

}

class DemoString extends FormattableString {
}

class DemoFormatter implements FormatterInterface {

  public function getFormattable($string, array $arguments = []) {
    return new DemoString($string, $arguments);
  }

  public function format($message, array $arguments) {
    // Deliberately not using the proper format to test.
    return call_user_func_array('sprintf', array_merge([$message], $arguments));
  }

}
