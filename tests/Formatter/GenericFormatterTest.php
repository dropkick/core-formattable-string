<?php

namespace Dropkick\Core\Formattable\Formatter;

use Dropkick\Core\Formattable\FormattableString;
use PHPUnit\Framework\TestCase;

class GenericFormatterTest extends TestCase {

  public function testGetFormattable() {
    $formatter = new GenericFormatter();
    $object = $formatter->getFormattable('Example with {{placeholder}}', ['placeholder' => 'success']);
    $this->assertEquals(FormattableString::class, get_class($object));
    $this->assertEquals('Example with success', (string)$object);
  }

  /**
   * @dataProvider providerFormats
   *
   * @param $string
   * @param $arguments
   * @param $expected
   */
  public function testFormats($string, $arguments, $expected) {
    $formatter = new GenericFormatter();
    $this->assertEquals($formatter->format($string, $arguments), $expected);
  }

  public function providerFormats() {
    // Empty string
    $formats[] = [
      '',
      [],
      ''
    ];

    // No placeholders.
    $formats[] = [
      'Example without {placeholder}',
      [],
      'Example without {placeholder}'
    ];

    // Undefined placeholder
    $formats[] = [
      'Example with {{placeholder}}',
      [],
      'Example with ',
    ];

    // Defined placeholder
    $formats[] = [
      'Example with {{placeholder}}',
      ['placeholder' => 'placeholder'],
      'Example with placeholder',
    ];

    // Ignored filters
    $formats[] = [
      'Example with {{placeholder|filter|optional("something")}}',
      ['placeholder' => 'placeholder'],
      'Example with placeholder',
    ];

    return $formats;
  }

}
