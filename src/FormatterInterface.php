<?php

namespace Dropkick\Core\Formattable;

/**
 * Interface FormatterInterface.
 *
 * A Formatter is the provider of the conversion between a string and its
 * placeholders into another string.
 *
 * While this can be used for any format transformation, having a standard
 * format will increase reuse of strings and translations.
 */
interface FormatterInterface {

  /**
   * Get a formattable string.
   *
   * Placeholders are identified by surrounding double curly brackets, and
   * may include optional filters, identified by separation of a pipe.
   *
   * @code
   *   'This is an {{ example | filter | optional("something") }} string'
   * @endcode
   *
   * The placeholder in the example is 'example', with two filters applied --
   * 'filter' which shows an argument-less filter, and 'optional("something")'
   * which shows a filter with arguments.
   *
   * Formatters should ignore unsupported, or unknown filters, outputting
   * solely the value of the placeholder.
   *
   * An empty string should replace an unspecified placeholder.
   *
   * @param string $string
   *   A string containing placeholder arguments.
   * @param array $arguments
   *   The values that will placed in the placeholders.
   *   This is indexed by identifying key.
   *
   * @return \Dropkick\Core\Formattable\FormattableInterface
   *   The formattable string.
   */
  public function getFormattable($string, array $arguments = []);

  /**
   * Formats a string.
   *
   * @param string $string
   *   The string containing placeholders.
   * @param array $arguments
   *   The placeholder arguments.
   *
   * @return string
   *   The resolved string.
   */
  public function format($string, array $arguments);

}
