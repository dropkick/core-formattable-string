<?php

namespace Dropkick\Core\Formattable\Formatter;

use Dropkick\Core\Formattable\FormattableString;
use Dropkick\Core\Formattable\FormatterInterface;

/**
 * Class GenericFormatter.
 *
 * The generic formatter provides only the most basic of formatting
 * functionality.
 */
class GenericFormatter implements FormatterInterface {

  /**
   * {@inheritdoc}
   */
  public function getFormattable($string, array $arguments = []) {
    return new FormattableString($string, $arguments);
  }

  /**
   * {@inheritdoc}
   */
  public function format($string, array $arguments) {
    $replacements = [];
    if (preg_match_all('/\{\{\s*([\w_-]+)(.*?)?\}\}/', $string, $matches, PREG_SET_ORDER)) {
      foreach ($matches as $match) {
        $placeholder = $match[1];
        $replacements[$match[0]] = $arguments[$placeholder] ?? '';
      }
    }
    return str_replace(array_keys($replacements), array_values($replacements), $string);
  }

}
