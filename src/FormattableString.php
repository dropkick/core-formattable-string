<?php

namespace Dropkick\Core\Formattable;

use Dropkick\Core\Formattable\Formatter\GenericFormatter;

/**
 * Class FormattableString.
 *
 * The base class used for providing formattable strings.
 */
class FormattableString implements FormattableInterface {

  /**
   * The default formatter used for string formatting.
   *
   * @var \Dropkick\Core\Formattable\FormatterInterface
   */
  protected static $defaultFormatter;

  /**
   * The placeholder string.
   *
   * @var string
   */
  protected $string;

  /**
   * The values for the placeholders.
   *
   * @var array
   */
  protected $arguments;

  /**
   * The internal formatter used for the formattable string.
   *
   * @var \Dropkick\Core\Formattable\FormatterInterface
   */
  protected $formatter;

  /**
   * FormattableTrait constructor.
   *
   * @param string $string
   *   The string containing placeholders.
   * @param array $arguments
   *   The values for the placeholders.
   * @param \Dropkick\Core\Formattable\FormatterInterface $formatter
   *   An optional specific formatter.
   */
  public function __construct($string, array $arguments = [], FormatterInterface $formatter = NULL) {
    $this->string = $string;
    $this->arguments = $arguments;
    $this->formatter = $formatter;
  }

  /**
   * {@inheritdoc}
   */
  public function getString() {
    return $this->string;
  }

  /**
   * {@inheritdoc}
   */
  public function getPlaceholders() {
    return $this->arguments;
  }

  /**
   * {@inheritdoc}
   */
  public function __toString() {
    return $this->format();
  }

  /**
   * {@inheritdoc}
   */
  public function format() {
    return $this->getFormatter()->format($this->string, $this->arguments);
  }

  /**
   * Get the formatter for this string.
   *
   * @return \Dropkick\Core\Formattable\FormatterInterface
   *   The instance formatter.
   */
  protected function getFormatter() {
    return $this->formatter ?? self::getDefaultFormatter();
  }

  /**
   * Get the default formatter used when no formatter passed to the string.
   *
   * @return \Dropkick\Core\Formattable\FormatterInterface
   *   The default formatter.
   */
  public static function getDefaultFormatter() {
    if (!isset(self::$defaultFormatter)) {
      self::$defaultFormatter = new GenericFormatter();
    }
    return self::$defaultFormatter;
  }

  /**
   * Set the default formatter for strings.
   *
   * @param \Dropkick\Core\Formattable\FormatterInterface $formatter
   *   The formatter to be used as the default.
   */
  public static function setDefaultFormatter(FormatterInterface $formatter = NULL) {
    self::$defaultFormatter = $formatter;
  }

  /**
   * Creates a string using the default formatter.
   *
   * @param string $string
   *   The string containing placeholders.
   * @param array $arguments
   *   The values for the placeholders.
   *
   * @return \Dropkick\Core\Formattable\FormattableInterface
   *   The created formattable.
   */
  public static function create($string, array $arguments = []) {
    return self::getDefaultFormatter()->getFormattable($string, $arguments);
  }

}
