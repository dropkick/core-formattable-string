<?php

namespace Dropkick\Core\Formattable;

/**
 * Interface FormattableInterface.
 *
 * The definition of a string that can be formatted in different ways.
 */
interface FormattableInterface {

  /**
   * The unadulterated string containing the placeholders.
   *
   * @return string
   *   The string containing placeholders.
   */
  public function getString();

  /**
   * The placeholders that need to be satisfied for the string to be formatted.
   *
   * @return string[]
   *   The placeholder values.
   */
  public function getPlaceholders();

  /**
   * Get the formatted string.
   *
   * @return string
   *   The formatted string.
   */
  public function format();

  /**
   * The formatted string, using format().
   *
   * @return string
   *   The formatted string.
   */
  public function __toString();

}
